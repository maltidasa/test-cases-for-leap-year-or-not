package com.Leap_year.Examples;

public class LeapYearClass {

	private int year;

	public LeapYearClass(int year) {
		this.year = year;
		
	}
	public int CheckLeapYear() {
		if(year%400==0) {
			System.out.println(year +" is Leap Year");
			return 1;}
		else if(year%100==0 && year %400 !=0) {
			System.out.println(year +" is Not Leap Year");
			return 0;}
		else if (year%100!=0 && year %4==0) {
			System.out.println(year +" is Leap Year");
			return 1;}
		else {
			System.out.println(year +" is Not Leap Year");
		return 0;}
	}
	

}
